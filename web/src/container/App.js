import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import AddForm from "../components/AddForm/AddForm";
import MainPage from "../components/MainPage/MainPage";
import ToolBar from "../components/Navigation/ToolBar/ToolBar";
import Orders from "../components/Orders/Orders";
import './App.css';

class App extends Component {
    render() {
        return (
            <Fragment>
                <ToolBar/>

            <div className="App" style={{paddingTop: '100px'}}>
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                    <Route path="/add" component={AddForm}/>
                    <Route path="/orders" component={Orders}/>
                </Switch>
            </div>
            </Fragment>

        );
    }
}

export default App;
