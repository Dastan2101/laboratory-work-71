
export const INIT_REQUEST = 'INIT_REQUEST';
export const INIT_SUCCESS = 'INIT_SUCCESS';
export const INIT_FAILURE = 'INIT_FAILURE';

export const INIT_ORDERS_SUCCESS = 'INIT_ORDERS_SUCCESS ';

export const initRequest = () => ({type: INIT_REQUEST});
export const initSuccess = (dishes) => ({type: INIT_SUCCESS, dishes});
export const initFailure = (error) => ({type: INIT_FAILURE, error});

export const initOrdersSuccess = (orders) => ({type: INIT_ORDERS_SUCCESS , orders});

