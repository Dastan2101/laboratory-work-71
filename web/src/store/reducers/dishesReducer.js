import {INIT_SUCCESS} from "../actions/actionType";

const initialState = {
    dishes: null,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {

        case INIT_SUCCESS:

            return {...state, dishes: action.dishes};

        default:
            return state
    }
};


export default dishesReducer