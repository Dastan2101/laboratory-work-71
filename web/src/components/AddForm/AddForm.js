import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {createDish} from "../../store/actions/action-creator";
import {connect} from "react-redux";

class AddForm extends Component {
    state = {
        title: '',
        price: '',
        image: ''
    };

    valueChanged = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    addDish = (event) => {

        event.preventDefault();
        const dish = {
            title: this.state.title,
            price: this.state.price,
            image: this.state.image
        };

        this.props.createDish(dish);

        this.setState({...this.state, title: '', price: '', image: ''});

    };

    render() {
        return (
            <div style={{width: '500px', margin: '20px auto', padding: '100px 0', color: 'white'}}>
                <h2>Add new dish</h2>
                <Form className="DishesForm" onSubmit={this.addDish}>
                    <FormGroup row>
                        <Label for="name" sm={2}>Product Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="title"
                                   id="name"
                                   value={this.state.title}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Image</Label>
                        <Col sm={10}>
                            <Input type="url" name="image"
                                   value={this.state.image}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Price</Label>
                        <Col sm={10}>
                            <Input type="number" name="price"
                                   value={this.state.price}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createDish: (dish) => dispatch(createDish(dish))
});

export default connect(null, mapDispatchToProps)(AddForm);