import {initFailure, initRequest, initSuccess} from "./actionTypes";
import axios from "../../axios-pizza";


export const getDishes = () => {
    return dispatch=> {
        dispatch(initRequest());
        axios.get('pizza.json').then(response => {
            dispatch(initSuccess(response.data));
        },error => {
            dispatch(initFailure(error))
        })
    }
};

export const postOrder = (order) => {
    return dispatch => {
        dispatch(initRequest());
        axios.post('pizza-orders.json',order).then(() => {
            dispatch(getDishes())
        })
    }
};
