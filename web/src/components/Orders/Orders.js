import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {getDishes, getOrders, removeOrder} from "../../store/actions/action-creator";

class Orders extends Component {

    componentDidMount() {
        this.props.getDishes();
        this.props.getOrders();

    }

    componentDidUpdate(prevProps) {
        if (prevProps.orders !== this.props.orders) {
            this.props.getOrders();
        }
    }

    render() {
        let orders;
        if (!this.props.dishes || !this.props.orders) {
            return <div>Loading...</div>
        }

        orders = Object.keys(this.props.orders).map((orderId, orderKey) => {
            let orderPrice = 0;
            let order = this.props.orders[orderId];

            let orderItem = Object.keys(order).map((dishId, key) => {
                if (this.props.dishes[dishId]) {

                    const dish = this.props.dishes[dishId];
                    const dishQty = order[dishId];
                    const orderItemPrice = dish.price * dishQty;

                    orderPrice += orderItemPrice;

                    if (dishQty === 0) {
                        return null
                    } else {
                        return (
                            <p key={key} style={{color: 'white', fontSize: '20x'}}>
                                <strong>{dish.title}</strong>: {dishQty} = {orderItemPrice} KGS</p>
                        )
                    }
                } else {
                    return (
                        <p key={key} style={{color: 'white', fontSize: '20x'}}>Вы удалили id в существующем заказе</p>
                    )
                }

            });
            return (
                <div key={orderKey} style={{margin: '20px', border: '5px solid white', textAlign: 'center'}}>
                    <p style={{color: 'white', fontSize: '20x'}}><strong>Order ID: {orderId}</strong></p>
                    {orderItem}
                    <p style={{color: 'white', fontSize: '20x'}}><strong>Delivery price: </strong> 150 KGS</p>
                    <p style={{color: 'white', fontSize: '20x'}}><strong>Total price: </strong> {orderPrice + 150} KGS
                    </p>
                    <button type="button" onClick={() => this.props.removeOrder(orderId)}>Complete order</button>
                </div>
            )

        });

        return (
            <div style={{width: '1000px', margin: '50px auto', border: '2px solid black'}}>
                {orders}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    orders: state.orders.orders
});

const mapDispatchToProps = dispatch => ({
    getOrders: () => dispatch(getOrders()),
    getDishes: () => dispatch(getDishes()),
    removeOrder: (id) => dispatch(removeOrder(id))

});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);