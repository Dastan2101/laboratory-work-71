import axios from '../../axios-pizza';
import {initSuccess, initRequest, initFailure, initOrdersSuccess} from "./actionType";

export const getDishes = () => {
    return dispatch=> {
        dispatch(initRequest());
        axios.get('pizza.json').then(response => {
            dispatch(initSuccess(response.data));
        },error => {
            dispatch(initFailure(error))
        })
    }
};

export const getOrders = () => {
    return dispatch=> {
        dispatch(initRequest());
        axios.get('pizza-orders.json').then(response => {
            dispatch(initOrdersSuccess(response.data));
        },error => {
            dispatch(initFailure(error))
        })
    }
};

export const createDish= (dish) => {
    return dispatch => {
        dispatch(initRequest());
        axios.post('pizza.json', dish).then(
            error => dispatch(initFailure(error))
        );
    }
};

export const removeDish = (id) => {
    return dispatch => {
        dispatch(initRequest());
        axios.delete('pizza/' + id + '.json').then(() => {
            dispatch(getDishes())
        })
    }
};

export const editDish = (id, change) => {
    return dispatch => {
        dispatch(initRequest());
        axios.put('pizza/' + id + '.json', change).then(() => {
            dispatch(getDishes())
        })
    }
};

export const removeOrder = (id) => {
    return dispatch => {
        dispatch(initRequest());
        axios.delete('pizza-orders/' + id + '.json').then(() => {
            dispatch(getDishes())
        })
    }
};