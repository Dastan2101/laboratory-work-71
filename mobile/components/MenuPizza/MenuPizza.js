import React from 'react';
import {StyleSheet, View, FlatList, TouchableOpacity, Text, Modal, ScrollView} from 'react-native';
import {getDishes, postOrder} from "../../store/actions/actionCreator";
import {connect} from "react-redux";
import MenuItem from "./MenuItem/MenuItem";
import {addDish, removeDish} from "../../store/actions/actionTypes";

class MenuPizza extends React.Component {

    state = {
        modalVisible: false,
    };

    toggleModal = () => {
        this.setState({modalVisible: !this.state.modalVisible});
    };

    createPost = post => {
        this.props.postOrder(post);
        this.toggleModal()
    };


    componentDidMount() {
        this.props.getDishes()
    }

    render() {

        let buttonCheckout = <TouchableOpacity style={{
            padding: 15,
            marginTop: 5,
            marginBottom: 10,
            height: 70,
            backgroundColor: '#ff140a',
            flexDirection: 'row',
            alignItems: 'center'
        }}
                                               onPress={this.toggleModal}
        >
            <Text style={{fontSize: 20, color: 'white'}}>Checkout</Text>
        </TouchableOpacity>;

        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.dishes}
                    renderItem={({item}) => <MenuItem addDish={this.props.addDish} item={item}/>}
                    keyExtractor={(item) => item.id}
                />


                <View style={{
                    height: 70,
                    padding: 15,
                    marginTop: 10,
                    backgroundColor: '#969EC8',
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <Text style={{
                        fontSize: 20,
                        color: '#fffbf9',

                    }}>
                        Order total: {this.props.totalPrice} KGS
                    </Text>
                </View>

                {this.props.active ? buttonCheckout : null}

                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    onRequestClose={this.toggleModal}>

                    <ScrollView style={{marginTop: 10, backgroundColor: '#fff', padding: 5}}>
                        <View style={{flex: 'row'}}>
                            {this.props.dishes.map(order => {

                                if (this.props.orders[order.id] < 1) {
                                    return null
                                }
                                return (
                                    <View key={order.id} style={styles.orderBlock}>
                                        <Text style={styles.text}>Dish : {order.title}</Text>
                                        <Text style={styles.text}>Quantity : {this.props.orders[order.id]}</Text>
                                        <Text style={styles.text}>Price
                                            : {order.price * this.props.orders[order.id]}</Text>
                                        <TouchableOpacity
                                            onPress={() => this.props.removeDish({id: order.id, price: order.price})}
                                            style={styles.btnOrder}>
                                            <Text style={{fontSize: 16, fontWeight: 'bold'}}>Remove</Text>
                                        </TouchableOpacity>
                                    </View>

                                )
                            })}

                            <View>
                                <View>
                                    <Text style={{fontSize: 20, fontWeight: 'bold', marginTop: 10}}>Delivery: 150
                                        KGS</Text>
                                    <Text style={styles.titleTotal}>Total
                                        price: {this.props.totalPrice + 150} KGS</Text>
                                </View>

                                <TouchableOpacity style={styles.btn} onPress={this.toggleModal}>
                                    <Text style={{fontSize: 30, fontWeight: 'bold'}}
                                    >Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btn}
                                                  onPress={() => this.createPost(this.props.orders)}
                                >
                                    <Text style={{fontSize: 30, fontWeight: 'bold'}}>Order</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>

                </Modal>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingTop: 30
    },
    text: {
        fontSize: 18,
        color: '#2b2b2b',
        marginTop: 5
    },
    titleTotal: {
        fontSize: 25,
        color: '#ff1919',
        marginTop: 20,
        fontWeight: 'bold'

    },
    orderBlock: {
        borderWidth: 1,
        borderColor: '#2b2b2b',
        padding: 20
    },
    btn: {
        padding: 20,
        marginTop: 10,
        height: 70,
        backgroundColor: '#ff140a',
        flexDirection: 'row',
        alignItems: 'center'
    },
    btnOrder: {
        padding: 10,
        marginTop: 5,
        height: 50,
        backgroundColor: '#5afffc',
        flexDirection: 'row',
        alignItems: 'center',
    }
});

const mapStateToProps = state => ({
    dishes: state.dishes,
    orders: state.orders,
    totalPrice: state.totalPrice,
    active: state.active
});

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(getDishes()),
    addDish: (dish) => dispatch(addDish(dish)),
    removeDish: (dish) => dispatch(removeDish(dish)),
    postOrder: (order) => dispatch(postOrder(order))
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuPizza)