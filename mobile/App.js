import React from 'react';
import {Provider} from "react-redux";
import MenuPizza from "./components/MenuPizza/MenuPizza";
import reducer from "./store/reducer/reducer";
import {StyleSheet, View} from "react-native";
import {applyMiddleware, compose, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore(reducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <View style={styles.container}>
                <MenuPizza />
            </View>
        </Provider>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

