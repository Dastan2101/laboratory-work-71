import React from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Col, Input, Form} from 'reactstrap';
import {connect} from "react-redux";
import {editDish} from "../../../store/actions/action-creator";

class ModalExample extends React.Component {
    state = {
        modal: false,
        title: '',
        image: '',
        price: ''
    };

       valueChanged = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };


    toggle = () => {

        this.setState({
            modal: !this.state.modal,

        });

    };

    changeToSend = (event) => {
      event.preventDefault();

        const change = {
            title: this.state.title,
            price: this.state.price,
            image: this.state.image
        };

        this.props.editDish(this.props.id, change);

        this.setState({...this.state, title: '', price: '', image: ''});

    };


    render() {
        return (
            <div>
                <Button color="success" onClick={this.toggle} style={{
                    height: '32px',
                    padding: '0 10px',
                    float: 'right',
                    margin: '-67px 90px 0 0'
                }}>Edit</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Edit Menu</ModalHeader>
                    <ModalBody>
                        <Form className="DishesForm">
                            <FormGroup row>
                                <Label for="name" sm={2}>Product Name</Label>
                                <Col sm={10}>
                                    <Input type="text" name="title"
                                           id="name"
                                           value={this.state.title || ''}
                                           onChange={this.valueChanged}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="category" sm={2}>Image</Label>
                                <Col sm={10}>
                                    <Input type="url" name="image"
                                           value={this.state.image || ''}
                                           onChange={this.valueChanged}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="category" sm={2}>Price</Label>
                                <Col sm={10}>
                                    <Input type="number" name="price"
                                           value={this.state.price || ''}
                                           onChange={this.valueChanged}
                                    />
                                </Col>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.changeToSend}>Save</Button>
                        <Button color="secondary" onClick={this.toggle}>Exit</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    title: state.dishes.title,
    image: state.dishes.image,
    price: state.dishes.price,
});

const mapDispatchToProps = dispatch => ({
    editDish: (id, change) => dispatch(editDish(id, change))
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalExample);