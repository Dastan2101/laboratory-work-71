import {ADD_DISH, INIT_SUCCESS, REMOVE_DISH} from "../actions/actionTypes";

const initialState = {
    dishes: [],
    orders: {},
    totalPrice: 0,
    active: false,
};

const dishesReducer = (state = initialState, action) => {

    switch (action.type) {
        case INIT_SUCCESS:
            let initDishes = Object.keys(action.dishes).map(id => {
                return {...action.dishes[id], id}
            });

            let newOrders = {};
            Object.keys(action.dishes).forEach((qty) => {
                newOrders[qty] = 0;
            });
            return {...state, dishes: initDishes, orders: newOrders, active: false, totalPrice: 0};

        case ADD_DISH:
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [action.dish.id]: state.orders[action.dish.id] + 1
                },
                totalPrice: state.totalPrice + parseInt(action.dish.price),
                active: true
            };
        case REMOVE_DISH:
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [action.dish.id]: state.orders[action.dish.id] - 1,

                },
                totalPrice: state.totalPrice - parseInt(action.dish.price)
            };
            
    }
    return state;
};


export default dishesReducer;