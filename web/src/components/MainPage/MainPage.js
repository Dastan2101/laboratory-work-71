import React, {Component} from 'react';
import {connect} from "react-redux";
import {getDishes, getOrders, removeDish} from "../../store/actions/action-creator";
import {Button, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import Modal from '../UI/Modal/Modal';


class MainPage extends Component {

    componentDidMount() {
        this.props.getDishes();
    }



    render() {
        let dishes;
        let initDishes;
        if (this.props.dishes) {
            initDishes = Object.keys(this.props.dishes).map(id => {
                return {...this.props.dishes[id], id}
            });
            dishes = initDishes.map((dish, ndx) => {

                return (
                    <div style={{border: '3px solid white', padding: '10px', margin: '5px 0'}} key={ndx}>
                        <img src={dish.image} alt="" style={{width: '100px', height: '100px'}}/>
                        <span style={{margin: ' 5px 15px', color: 'white', fontWeight: 'bold', fontSize: '22px'}}>{dish.title}</span>
                        <span style={{color: 'white', fontWeight: 'bold',fontSize: '20px'}}>{dish.price} KGS</span>
                        <Button color="danger" size="sm" style={{float: 'right', marginTop: '33px'}}
                                onClick={() => this.props.removeDish(dish.id)}>Delete</Button>
                        <Modal id={dish.id}/>
                    </div>
                )

            })
        }
        return (
            <div style={{width: '95%', margin: '5px auto'}}>
                <h2>Dishes</h2>
                <Button color="secondary" style={{float: 'right', marginTop: '-65px'}}>
                    <NavLink to="/add"
                             tag={RouterNavLink}
                             exact
                             style={{color: '#fff'}}
                    >
                        Add new
                        dish
                    </NavLink>
                </Button>
                {dishes}
            </div>

        )
            ;
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes
});

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(getDishes()),
    getOrders: () => dispatch(getOrders()),
    removeDish: (id) => dispatch(removeDish(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);