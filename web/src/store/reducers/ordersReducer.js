import {INIT_ORDERS_SUCCESS} from "../actions/actionType";

const initialState = {
    orders: null
};

const ordersReducer = (state = initialState, action) => {
    switch (action.type) {

        case INIT_ORDERS_SUCCESS:

            return {...state, orders: action.orders};

        default:
            return state
    }

};

export default ordersReducer;