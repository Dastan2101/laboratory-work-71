import React from 'react';
import NavigationItems from "../NavigationItems/NavigationItems";
import './ToolBar.css';

const ToolBar = () => (
    <header className="Toolbar">
        <nav>
            <NavigationItems/>
        </nav>
    </header>
);

export default ToolBar;