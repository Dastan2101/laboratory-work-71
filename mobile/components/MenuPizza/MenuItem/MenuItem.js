import {Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';


const MenuItem = ({item, addDish}) => (
    <TouchableOpacity onPress={() => addDish({id: item.id, price: item.price})}>
        <View style={{
            padding: 15,
            marginTop: 10,
            height: 100,
            backgroundColor: '#2b2b2b',
            flexDirection: 'row',
            alignItems: 'center'
        }}

        >
            <Image
                source={{uri: item.image}}
                style={{width: 50, height: 50, marginRight: 10, borderRadius: 2}}
            />
            <Text style={{marginRight: 10, fontSize: 18, color: '#fff'}}>{item.title}</Text>
            <Text style={{marginRight: 10, fontSize: 18, color: '#fff'}}>{item.price} KGS</Text>
        </View>
    </TouchableOpacity>
);

export default MenuItem;
