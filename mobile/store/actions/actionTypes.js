export const INIT_REQUEST = 'INIT_REQUEST';
export const INIT_SUCCESS = 'INIT_SUCCESS';
export const INIT_FAILURE = 'INIT_FAILURE';
export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';

export const addDish = dish => ({type: ADD_DISH, dish});

export const removeDish = dish => ({type: REMOVE_DISH, dish});

export const initRequest = () => ({type: INIT_REQUEST});
export const initSuccess = (dishes) => ({type: INIT_SUCCESS, dishes});
export const initFailure = (error) => ({type: INIT_FAILURE, error});